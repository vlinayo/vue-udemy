new Vue({
        el: '#exercise2',
        data: {
            value: ''
        },
        methods: {
            showAlert: function(){
                alert('This is an alert!');
            },
            storeValue: function(event){
                this.value = event.target.value;
            }
        }
    });