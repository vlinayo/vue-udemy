// var data= { status: 'Critical'};
//de esta forma se tienen dos instancias con el objeto DATA

var cmp = {
    data: function() {
        return {
            status: 'Critical'
        }
    },
    template: '<p>Server Status: {{ status }} (<button @click="changeStatus">Change </button>) </p>',
    methods: {
        changeStatus: function() {
            this.status = 'Normal'
        } 
    }
}
// Vue.component('my-cmp',{
//     data: function() {
//         return data;
//     },
//     template: '<p>Server Status: {{ status }} (<button @click="changeStatus">Change </button>) </p>',
//     methods: {
//         changeStatus: function() {
//             this.status = 'Normal'
//         } //de esta forma cambiamos el status de todos nuestros objetos data!
//     }
// });

new Vue({
    el: '#app',
    components: {
        'my-cmp': cmp
    }
})

new Vue({
    el: '#app2'
})